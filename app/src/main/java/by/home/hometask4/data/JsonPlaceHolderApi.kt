package by.home.hometask4.data

import by.home.hometask4.utilities.GET_GOMEL_ATMs_PARAMETER
import retrofit2.Call
import retrofit2.http.GET

interface JsonPlaceHolderApi {
    @GET(GET_GOMEL_ATMs_PARAMETER)
    fun getGomelAtms(): Call<List<ATM>>
}