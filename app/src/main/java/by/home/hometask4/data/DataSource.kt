package by.home.hometask4.data

import android.content.Context
import android.widget.Toast
import by.home.hometask4.R
import by.home.hometask4.utilities.BELARUSBANK_URL
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DataSource(val context: Context, val map: GoogleMap) {
    private val retrofit = Retrofit.Builder().baseUrl(BELARUSBANK_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val jsonPlaceHolderApi: JsonPlaceHolderApi =
        retrofit.create(JsonPlaceHolderApi::class.java)

    private val listCall = jsonPlaceHolderApi.getGomelAtms()

    fun addMarkers() {

        listCall.enqueue(
            object : Callback<List<ATM>> {
                override fun onResponse(call: Call<List<ATM>>, response: Response<List<ATM>>) {
                    if (!response.isSuccessful) {
                        Toast.makeText(
                            context,
                            "Code: " + response.code(),
                            Toast.LENGTH_LONG
                        )
                            .show()
                        return
                    }

                    val atmsList: List<ATM> = response.body()!!



                    atmsList.forEach {
                        val mapSnippetText = """
                        ${it.addressType} ${it.address}, ${it.house}
                        
                        График работы: ${it.workTime}
                        
                        Выдаваемая валюта: ${it.currency}
                    """.trimIndent()

                        map.addMarker(
                            MarkerOptions()
                                .icon(
                                    BitmapDescriptorFactory
                                        .fromResource(R.drawable.baseline_local_atm_black_24)
                                )
                                .position(LatLng(it.gpsX, it.gpsY))
                                .title(it.installPlace)
                                .snippet(mapSnippetText)
                        )
                    }
                }

                override fun onFailure(call: Call<List<ATM>>, t: Throwable) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }
            })
    }

}