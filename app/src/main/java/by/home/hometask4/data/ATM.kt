package by.home.hometask4.data

import by.home.hometask4.enums.ATMCurrency
import com.google.gson.annotations.SerializedName

class ATM(
    @SerializedName("id")
    val id: Int,

    @SerializedName("address_type")
    val addressType: String,

    @SerializedName("address")
    val address: String,

    @SerializedName("house")
    val house: String,

    @SerializedName("install_place")
    val installPlace: String,

    @SerializedName("work_time")
    val workTime: String,

    @SerializedName("gps_x")
    val gpsX: Double,

    @SerializedName("gps_y")
    val gpsY: Double,

    @SerializedName("ATM_type")
    val atmType: String,

    @SerializedName("currency")
    val currency: ATMCurrency,
)