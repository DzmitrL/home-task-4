package by.home.hometask4.utilities

const val BELARUSBANK_URL = "https://belarusbank.by/"
const val GET_GOMEL_ATMs_PARAMETER = "api/atm?city=Гомель"

const val GOMEL_LATITUDE = 52.440513
const val GOMEL_LONGITUDE = 30.988011

const val GOMEL_MAP_ZOOM = 11F