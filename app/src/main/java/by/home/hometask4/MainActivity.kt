package by.home.hometask4

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import by.home.hometask4.adapters.CustomInfoWindowForGoogleMap
import by.home.hometask4.data.DataSource
import by.home.hometask4.utilities.GOMEL_LATITUDE
import by.home.hometask4.utilities.GOMEL_LONGITUDE
import by.home.hometask4.utilities.GOMEL_MAP_ZOOM
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng

class MainActivity : FragmentActivity(), OnMapReadyCallback {
    private lateinit var map: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mapFragment: SupportMapFragment =
            supportFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        val gomel = LatLng(GOMEL_LATITUDE, GOMEL_LONGITUDE)

        DataSource(this, map).addMarkers()
        map.setInfoWindowAdapter(CustomInfoWindowForGoogleMap(this))
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, GOMEL_MAP_ZOOM))
    }
}