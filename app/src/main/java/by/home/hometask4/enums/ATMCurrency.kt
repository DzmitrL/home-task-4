package by.home.hometask4.enums

enum class ATMCurrency {
    BYN,
    USD,
    EUR
}